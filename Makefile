# Variables
ANSIBLE_PLAYBOOK ?= deploy_k3s.yml
INVENTORY_FILE ?= inventory_local.yaml
ANSIBLE_LINT_IMAGE ?= pipelinecomponents/ansible-lint
DOCKER_IMAGE ?= alpine/ansible:2.17.0

# Règles
.PHONY: lint test syntax-check

all: lint syntax-check test

lint:
	@echo "🔍🔍🔍 TEST 1/4: Linting YAML files..."
	yamllint .
	@echo "✅✅✅  YAML linting passed."

	@echo "🔍🔍🔍 TEST 2/4: Linting Ansible playbooks..."
	docker run --rm -v $(PWD):/code $(ANSIBLE_LINT_IMAGE) $(ANSIBLE_PLAYBOOK)
	@echo "✅✅✅  Ansible linting passed."

syntax-check:
	@echo "🔍🔍🔍 TEST 3/4: Running Ansible syntax check..."
	docker run --rm -v $(PWD):/data $(DOCKER_IMAGE) /bin/sh -c "cd /data && ansible-playbook -i $(INVENTORY_FILE) $(ANSIBLE_PLAYBOOK) --vault-password-file key --syntax-check"
	@echo "✅✅✅  Syntax check passed."

test:
	@echo "🔍🔍🔍 TEST 4/4: Running Ansible playbook in check mode..."
	docker run --rm -v $(PWD):/data -e ANSIBLE_FORCE_COLOR=true $(DOCKER_IMAGE) /bin/sh -c "cd /data && ansible-playbook -i $(INVENTORY_FILE) $(ANSIBLE_PLAYBOOK) --vault-password-file key --check"
	@echo "✅✅✅  Check mode test passed."