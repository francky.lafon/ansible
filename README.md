Déploiement des repository git
=========

Ceci est un role Ansible personnel pour simplifier la gestion de tout mes repository sur tout mes devices.

Requirements
------------

- Pensez à mettre la clé SSH des hosts dans le gitlab

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Playbook
--------

- 00-code_gitlab

- 01-code_server

License
-------

GNU-GPL

Author Information
------------------

Franck Lafon
