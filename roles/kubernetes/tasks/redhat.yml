---
- name: All_kube_node
  block:
    - name: Reset | Reset kubernetes cluster
      ansible.builtin.command: "kubeadm reset -f --cri-socket=/var/run/crio/crio.sock"
      ignore_errors: true

    - name: Reset | Remove packages
      ansible.builtin.package:
        name: "{{ kubernetes_packages }}"
        state: absent

    - name: Firewalld | Copy file for repository
      ansible.builtin.copy:
        src: "{{ item.src }}"
        dest: "{{ item.dest }}"
        owner: root
        group: root
        mode: '0644'
      loop:
        - {src: 'cri-o.repo', dest: '{{ kubernetes_yum_folder }}/cri-o.repo'}
        - {src: 'kubernetes.repo', dest: '{{ kubernetes_yum_folder }}/kubernetes.repo'}
        - {src: 'kube-controller-manager.xml', dest: '{{ kubernetes_firewalld_folder }}/kube-controller-manager.xml'}
        - {src: 'kube-scheduler.xml', dest: '{{ kubernetes_firewalld_folder }}/kube-scheduler.xml'}
        - {src: 'kubelet-api.xml', dest: '{{ kubernetes_firewalld_folder }}/kubelet-api.xml'}
        - {src: 'kubelet-etcd.xml', dest: '{{ kubernetes_firewalld_folder }}/kubelet-etcd.xml'}
        - {src: 'kubernetes-api.xml', dest: '{{ kubernetes_firewalld_folder }}/kubernetes-api.xml'}

    - name: CRI-O | Import from a url
      ansible.builtin.rpm_key:
        key: " {{ kubernetes_crio_url }} "
        state: present
      when: ansible_distribution == "RedHat"

    - name: CRI-O | Install packages
      ansible.builtin.dnf:
        name: cri-o
        state: latest    # noqa package-latest
      when: not ansible_check_mode

    - name: CRI-O | Service
      ansible.builtin.systemd:
        name: crio
        state: started
        enabled: true

    - name: Install_packages_kubernetes
      ansible.builtin.package:
        name: ['kubeadm', 'kubelet', 'kubectl', 'runc']
        state: present
        disable_gpg_check: true
      when: not ansible_check_mode

    # a revoir avec l'install officiel et check ansible-lint
    - name: Cmd_modprobe_systcl_swapoff
      ansible.builtin.command: "{{ item }}"
      with_items:
        - modprobe br_netfilter
        - sysctl -w net.ipv4.ip_forward=1
        - sysctl -p
        - swapoff -a

    - name: Creates_an_entry_crontab_for_swapoff
      ansible.builtin.cron:
        name: "swapoff -a"
        special_time: reboot
        job: /usr/sbin/swapoff -a
        state: present

    - name: Enable service kubelet
      ansible.builtin.systemd:
        name: kubelet
        enabled: true

    - name: Pulling images required for setting up a Kubernetes cluster
      ansible.builtin.command: kubeadm config images pull

    - name: Apply_firewalld_masquerade
      ansible.posix.firewalld:
        masquerade: true
        permanent: true
        state: enabled
        zone: public

###
- name: On kube_master
  when: kube_master is defined and kube_master
  block:
    - name: Kube-master | Apply_firewalld_master
      ansible.posix.firewalld:
        service: "{{ item }}"
        permanent: true
        state: enabled
      loop:
        - kube-controller-manager
        - kube-scheduler
        - kubelet-api
        - kubelet-etcd
        - kubernetes-api

    - name: Reload_service_firewall
      ansible.builtin.command: firewall-cmd --reload

    - name: Kube-master | Init Kubernetes cluster
      ansible.builtin.command: "kubeadm init --token {{ token_kube }} --cri-socket=/var/run/crio/crio.sock"
      register: kube_init

    - name: Kube-master | Create directory user .kube
      ansible.builtin.file:
        path: /home/franck/.kube
        state: directory
        owner: franck
        group: franck
        mode: '0700'

    - name: Kube-master | Export kubeconfig on user
      ansible.builtin.copy:
        src: /etc/kubernetes/admin.conf
        dest: /home/franck/.kube/config
        remote_src: true
        owner: franck
        group: franck
        mode: '0600'
      when: not ansible_check_mode

# ###
- name: On kube-worker
  when: kube_worker is defined and kube_worker
  block:
    - name: Kube-worker | Apply_firewalld_service
      ansible.posix.firewalld:
        service: kubelet-api
        permanent: true
        state: enabled

    - name: Kube-worker | Apply_firewalld_port
      ansible.posix.firewalld:
        port: 30000-32767/tcp
        permanent: true
        state: enabled

    - name: Reload_service_firewall
      ansible.builtin.command: firewall-cmd --reload

    - name: Kube-worker | Join_Kubernetes_cluster
      ansible.builtin.command: "kubeadm join --token {{ token_kube }} --discovery-token-unsafe-skip-ca-verification --cri-socket=unix://var/run/crio/crio.sock \
                                {{ node_master }}:6443"
      # when: kube_init.changed == false
